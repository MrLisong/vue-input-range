# vue-input-range

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# use point
这是一个基于vue的数字范围插件，该插件已发布在npm上，可在项目中下载使用，使用方法如下：
```
npm install vue-input-range --save

import inputRange from 'vue-input-range'

Vue.use(inputRange)

option:
  value: 绑定的值
  width: 输入框的宽度     默认值： 300
  size: 输入框尺寸（small|middle|large）       默认值： middle
  preLabelIcon：输入框前面的图标路径（需自定义）   默认值：无
  prePlaceholder：第一个输入框的占位文字         默认值：开始
  appendPlaceholder：第二个输入框的占位文字      默认值：结束
  connectText：两个输入框的连接文字             默认值：至

例：<vue-input-range :width="200" size="small"></vue-input-range>
```
