import MyInputRange from './input-ranger'
const InputRange = {
  install: (Vue) => {
    Vue.component('VueInputRange', MyInputRange);
  }
};
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(InputRange);
}
export default InputRange;
